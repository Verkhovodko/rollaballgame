﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	[SerializeField]public float speed;
	[SerializeField]public Text count_Text;
	[SerializeField]public Text win_Text;
	
	[SerializeField]private int jump;
	[SerializeField]private bool is_ground;

	[SerializeField]private int platform_jump;

	public GameObject bridge;

	private Rigidbody rb;
	private int count;

	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		bridge = GameObject.FindGameObjectWithTag("Bridge");
		bridge.SetActive(false);
		count = 0;
		Set_Count_Text();
		win_Text.text = "";
		jump = 250;
		platform_jump = 500;
	}

	void FixedUpdate ()
	{
		float move_Horizontal = Input.GetAxis("Horizontal");
		float move_Vertical = Input.GetAxis("Vertical");

		Vector3	movement = new Vector3	(move_Horizontal, 0.0f, move_Vertical);

		rb.AddForce(movement * speed);

		Jump();
	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);
			count += 1;
			Set_Count_Text();
		}

		if (other.gameObject.CompareTag("Platform"))
		{
			rb.AddForce(Vector3.up * platform_jump);
		}

		if (other.gameObject.CompareTag("BridgePlatform"))
		{
			bridge.SetActive(true);				
		}
    }

    void Jump()
    {
    	Ray ray = new Ray(gameObject.transform.position, Vector3.down);
    	RaycastHit rh;
    	if (Physics.Raycast(ray, out rh, 0.5f))
    	{
    		is_ground = true;
    	}
    	else
    	{
    		is_ground = false;
    	}

    	if (Input.GetKeyDown(KeyCode.Space) && is_ground)
    	{
    		rb.AddForce(Vector3.up * jump);
    	}
    }

    void Set_Count_Text()
    {
    	count_Text.text = "Count: " + count.ToString();
    	if (count >= 8)
    	{
    		win_Text.text = "You Win!";
    	}
    }
}